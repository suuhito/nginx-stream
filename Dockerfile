FROM golang:1.16 AS builder

COPY ./configure.sh /go/configure.sh

RUN go install github.com/cubicdaiya/nginx-build@master && \
    mkdir work && \
    nginx-build -d work -zlib -pcre -openssl -c configure.sh && \
    cp work/nginx/*/*/objs/nginx .

FROM nginx:alpine
COPY --from=builder /go/nginx /usr/local/bin/nginx
